import React from 'react';
import ReactDOM from 'react-dom';
import AppEntry from './components/AppEntry';

const render = () => {
    ReactDOM.render(
        <AppEntry name="Damian" />,
        document.getElementById('app')
    );
};

render();

if (module.hot) {
    module.hot.accept();
}
