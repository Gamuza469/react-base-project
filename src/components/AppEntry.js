import React, {Component} from 'react';

import {hot} from 'react-hot-loader/root';
import PropTypes from 'prop-types';
import {
    HashRouter,
    Link,
    Route,
    Switch
} from 'react-router-dom';

import GoodbyeMessage from './GoodbyeMessage';
import HelloMessage from './HelloMessage';

class AppEntry extends Component {
    render() {
        const {name} = this.props;

        return (
            <HashRouter>
                <div>
                    Current user logged in: {name}
                </div>
                <div>
                    <Link to="/">Home</Link>
                    {' '}
                    <Link to="/hello">Hello</Link>
                    {' '}
                    <Link to="/goodbye">Goodbye</Link>
                </div>
                <div>
                    <Switch>
                        <Route path="/hello">
                            <HelloMessage name={name} />
                        </Route>
                        <Route path="/goodbye">
                            <GoodbyeMessage name={name} />
                        </Route>
                    </Switch>
                </div>
            </HashRouter>
        );
    }
}

AppEntry.propTypes = {
    name: PropTypes.string
};

AppEntry.defaultProps = {
    name: 'user'
};

export default hot(AppEntry);
