import React, {Component} from 'react';

import {hot} from 'react-hot-loader/root';
import PropTypes from 'prop-types';

class HelloMessage extends Component {
    render() {
        const {name} = this.props;

        return (
            <div>
                Hello there, Mr. {name}!
            </div>
        );
    }
}

HelloMessage.propTypes = {
    name: PropTypes.string
};

HelloMessage.defaultProps = {
    name: 'user'
};

export default hot(HelloMessage);
