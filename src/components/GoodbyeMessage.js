import React, {Component} from 'react';

import {hot} from 'react-hot-loader/root';
import PropTypes from 'prop-types';

class GoodbyeMessage extends Component {
    render() {
        const {name} = this.props;

        return (
            <div>
                Goodbye, {name}!!
            </div>
        );
    }
}

GoodbyeMessage.propTypes = {
    name: PropTypes.string
};

GoodbyeMessage.defaultProps = {
    name: 'user'
};

export default hot(GoodbyeMessage);
