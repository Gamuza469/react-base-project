const express = require('express');
const webpack = require('webpack');

const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');

const config = require('../webpack.dev');

const compiler = webpack(config);
const app = express();

app.use(webpackDevMiddleware(compiler));
app.use(webpackHotMiddleware(compiler));

app.get('/*', (request, response) => {
    response.redirect('/');
});

app.listen(
    3000,
    () => console.info('Development server listening at port 3000.')
);
