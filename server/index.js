const path = require('path');
const express = require('express');

const app = express();
const DIST_PATH = path.join(__dirname, '..', '/dist');

app.use(
    express.static(DIST_PATH)
);

app.get('/', (request, response) => {
    response.sendFile(path.join(DIST_PATH, 'index.html'));
});

app.get('/*', (request, response) => {
    response.redirect('/');
});

app.listen(
    8080,
    () => console.info('Production server listening at port 8080.')
);
