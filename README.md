# React Base Project

A base project made for writing and presenting React.js based example learning projects. Implemented with the bare minimum dependencies. Oriented to those in need of a project to initiate and test-drive React.js code, examples and applications.

This project diferentiates from the **ReactJS quick tutorial** as this can be used directly as a base to build new React.js learning projects from the ground up. Instead of an obfuscated `create-react-app` automated script everything here is served from the start.

## Instructions

### First steps

This project uses Node.js as its foundation. Install it on your system and navigate to this folder, the project's root folder, in your CLI then run the following command:

```bash
npm install
```

This will install the required dependencies needed for this project to function properly.

### Running the development web server

This will be the main endpoint. Use the development web server to actively and interactively develop your React.js code.

1. Run the following command:

    ```bash
    npm run dev-server
    ```

2. Launch your browser at `http://localhost:3000`.
3. Code, save then see the updated app at your browser.

### Add a new component

Add your new component files inside the `src/components` folder then use them on either `src/index.js` (the initial `ReactDOM.render()` call) or `src/components/AppEntry.js` (to use them as child components).

> Don't forget to call `hot()` when exporting the components for **React Hot Loader** to work correctly.

### Building the app

If you want to build the app, meaning you've finished development and you want to share your work, you must follow these steps.

1. Run the following commands:

    ```bash
    npm run build
    npm run prod-server
    ```

2. Open your browser at the specified URLs (by default it should point to `http://localhost:8080`). It should work the same as when you were developing your app.
3. After you made sure everything is working as expected you can deploy the files found inside the `/dist` folder to your local or remote web server.

    > Do remember, if you want to continue coding at this point you must do so on the development web server. Once you finish, build again your app by repeating these steps.

## Project objectives

Apart from being a simple base React.js project, oriented to those who wish to make their first steps into learning how to code with React.js in an already built project, another of its goals is to guide new developers through the different parts and libraries which make the whole project and detail the reason or meaning behind the implementation of each part of it. Since most tutorials, courses or walkthroughs fail to cover the reasons for implementing, say, a specific library, this project will focus its efforts to detail the why's and where's.

That being said, first focus will be getting this up and running. After that, the explaning part will be implemented. Mainly in terms of (maybe) formal documentation.

Also, the project will implement several React.js component implementations in the form of both Class components and Functional components. Hooks will be used in some of the functional components.

## Project scope

The project is intended to implement a base project for developing React.js applications with the purpose of learning React.js. This means the base project will implement a learning development environment consisting of several key requirements:

- Active watch of all the project files and reload in the browser, in real-time, those files edited by the developer.
- Linting pre-processors to aid the developer in finding bugs and code lints to improve its coding abilities while learning to program.
- Commit linter to aid the developer in writing well-described commit messages.
- Implement common recommended coding style rules for code homogenization.

## Technologies

Small breakdown of all technologies used in this project.

### Main application

- React.js
- React Router

### Application building

- Webpack
- Babel.js

### Development server

- React-Hot-Loader
- Webpack Dev Middleware
- Webpack Hot Middleware

### Development aids

- ESLint
- Husky

### Development and production server

- Express.js

## What this project will implement

As this project will serve as a initial point for developing applications/learning projects it should have some common elements needed for most web application/web page projects:

- CSS compilers and libraries (Bootstrap, SASS)
- Logging (Winston)
- Commit linters and hooks (Husky, commitlint)
- Testing (Jest)

## What this project will not implement

To keep the scope to a reduced set of requirements only the technologies described above will be implemented. The following are parts that are out of the project's scope, and thus, do not serve the purpose of this project:

- Connect API
- Redux (and Redux-Sagas)
- ~~Express.js (for routing)~~
- Any ORM libraries (Mongoose, Knex.js, Sequelize)
- TypeScript
- Detailed error information
- Automated tasks (Gulp)

## File structure breakdown

- dist folder: after building the application, the static files found here are ready to be deployed.
- docs folder: all documentation related to this project, including reasoning for every decision made (coming soon).
- public folder: static elements such as images, HTML pages and others.
- server folder: it has the necessary logic for running the development and production servers.
- src folder: main project folder.

## Using it as a base project

Delete the files inside both the `src` and `dist` folders and you are good to go. Have in mind you will have to write your own new entry point.

## Known issues

~~React Hot Loader does not seem to reload PureComponents. Seems a regression occurred some time ago. Just change the component you want to extend React.Component instead of React.PureComponent.~~ - fixed. Still, it's better if you don't use PureComponents if you are still green developing with React.
